alter table tbl_entradas add anio integer as (year(fecha_entrada));
alter table tbl_entradas add mes integer as (month(fecha_entrada));

create table tbl_folios_entrada
(
	anio integer not null,
	mes integer not null,
	ultimo_folio integer not null,
	primary key(anio, mes)
);