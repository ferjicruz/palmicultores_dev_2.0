create table tbl_estados 
(
	id integer not null primary key auto_increment,
	clave char(2) NOT NULL,
	nombre varchar(160)	NOT NULL
);

create table tbl_municipios
(
	id integer not null primary key auto_increment,
	clave char(2) NOT NULL,
	nombre varchar(160) NOT NULL,
	estado_id integer NOT NULL
);

create table tbl_localidades
(
	id integer not null primary key auto_increment,
	clave char(4) NOT NULL,
	nombre varchar(160) NOT NULL,
	estado_id integer NOT NULL
);

create table tbl_productores
(
	id integer not null primary key auto_increment,
	clave numeric NOT NULL,
	nombre text(60) NOT NULL,
	apellido_paterno text(60) null,
  	apellido_materno text(60) null,
	estado_id integer null,
  	municipio_id integer null,
  	localidad_id integer null,
	no_parcelas integer NOT NULL,
  	curp varchar(18) NULL,
	rfc varchar(13) NULL,
 	calle varchar(60) NULL,
	no_exterior varchar(20) NULL,
	no_interior varchar(20) NULL,
 	colonia varchar(60) NULL,
	codigo_postal integer(5) NULL,
	referencia varchar(160) NULL,
  	correo_electronico varchar(50) NULL,
	telefono integer(10) NULL,
	celular integer(10) NULL
);

create table tbl_sociedades
(
	id integer not null primary key auto_increment,
	razon_social varchar(160) NOT NULL,
	no_miembros integer NOT NULL,
	fecha_creacion datetime,
	estado_id integer NULL,
  	municipio_id integer NULL,
  	localidad_id integer NULL,
	no_parcelas integer NOT NULL,
	rfc varchar(13) NULL,
 	calle varchar(160) NULL,
	no_exterior varchar(20) NULL,
	no_interior varchar(20) NULL,
 	colonia varchar(160) NULL,
	codifgo_postal integer(5) NULL,
	referencia varchar(160) NULL,
  	correo_electronico varchar(160) NULL,
	telefono integer(10) NULL,
	celular integer(10) NULL,
	representante_legal varchar(160) NULL
);

create table tbl_centros_acopio 
(
	id integer not null primary key auto_increment,
	nombre text(60) NOT NULL,
	no_miembros integer NOT NULL,
	estado_id integer NULL,
  	municipio_id integer NULL,
  	localidad_id integer NULL,
	no_parcelas integer NOT NULL,
 	calle varchar(160) NULL,
	no_exterior varchar(20) NULL,
	no_interior varchar(20) NULL,
 	colonia varchar(160) NULL,
	codigo_postal int(5) NULL,
	referencia varchar(160) NULL,
  	correo_electronico varchar(160) NULL,
	telefono integer(10) NULL,
	celular integer(10) NULL	
	);
    
alter table tbl_localidades
  add constraint fk_estados_localidades
	foreign key (estado_id) 
		references tbl_estados (id);
		
alter table tbl_municipios
  add constraint fk_estados_municipios
	foreign key (estado_id) 
		references tbl_estados (id);

alter table tbl_productores
  add constraint fk_estado_productores
	foreign key (estado_id) 
		references tbl_estados (id);
		
alter table tbl_productores
  add constraint fk_municipio_productores
    foreign key (municipio_id)
	  references tbl_municipios (id);
	  
alter table tbl_productores
  add constraint fk_localidad_productores
	foreign key (localidad_id)
	  references tbl_localidades (id);
	  
alter table tbl_sociedades
  add constraint fk_estado_sociedades
	foreign key (estado_id) 
		references tbl_estados (id);
		
alter table tbl_sociedades
  add constraint fk_municipio_sociedades
    foreign key (municipio_id)
	  references tbl_municipios (id);
	  
alter table tbl_sociedades
  add constraint fk_localidad_sociedades
	foreign key (localidad_id)
	  references tbl_localidades (id);z
	  
alter table tbl_centros_acopio
  add constraint fk_estado_centros_acopio
	foreign key (estado_id) 
		references tbl_estados (id);
		
alter table tbl_centros_acopio
  add constraint fk_municipio_centros_acopio
    foreign key (municipio_id)
	  references tbl_municipios (id);
	  
alter table tbl_centros_acopio
  add constraint fk_localidad_centros_acopio
	foreign key (localidad_id)
	  references tbl_localidades (id);	
	  
	  
	  
	  
	  /*
	
	create table tbl_entradas
(
	id integer not null primary key auto_increment,
	productor_id integer null,
	sociedad_id integer null,
	centro_acopio_id integer null,
	numero_entrada integer(10) not null,
	folio integer(10) not null,
	fecha_entrada date not null,
	hora_entrada time not null,
	peso_bruto decimal(12,2) not null,
	peso_tara decimal(12,2) not null,
	fecha_salida date null,
	hora_salida time null,
	vehiculo varchar(160) not null,
	placas varchar(30) not null,
	chofer varchar(160) not null,
	operador_bascula varchar(160) not null,
	observaciones varchar(300) not null,
	status_pagada char(1) not null	
);
	  */
	  