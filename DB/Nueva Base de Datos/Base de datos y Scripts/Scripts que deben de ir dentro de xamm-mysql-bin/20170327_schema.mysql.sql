create table tbl_entradas
(
	id integer not null primary key auto_increment,
	productor_id integer null,
	nombre text(60) NOT NULL,
	apellido_paterno text(60) null,
    dapellido_materno text(60) null,
	sociedad_id varchar(20) null,
	centro_acopio_id varchar(20) null,
	numero_entrada integer(5) not null,
	folio integer not null,
	fecha_entrada date not null,
	hora_entrada time not null,
	peso_bruto decimal(12,2) not null,
	peso_tara decimal(12,2) not null,
	fecha_salida date null,
	hora_salida time null,
	vehiculo varchar(50) not null,
	placas varchar(30) not null,
	chofer varchar(50) not null,
	operador_bascula varchar(160) not null,
	observaciones varchar(300) not null,
	status_pagada varchar(20) not null
);

alter table tbl_entradas
	add constraint fk_productor_entrada
		foreign key (productor_id)
			references tbl_productores (id);

alter table tbl_entradas
	add constraint fk_sociedad_entrada
		foreign key (sociedad_id)
			references tbl_sociedades (id);

alter table tbl_entradas
	add constraint fk_centro_acopio_entrada
		foreign key (centro_acopio_id)
			references tbl_centros_acopio (id);
