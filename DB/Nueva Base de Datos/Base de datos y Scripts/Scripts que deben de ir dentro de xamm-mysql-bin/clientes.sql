create table tbl_clientes
(
	id integer not null primary key auto_increment,
	nombre text(30)	NOT NULL,
	apellido_paterno text(30) null,
  	apellido_materno text(30) null,
	calle varchar(50) NULL,
	no_exterior varchar(20) NULL,
	no_interior varchar(20) NULL,
 	colonia varchar(50) NULL,
  	correo_electronico varchar(30) null,
	telefono int(10) null,
	rfc char(13) null
)
