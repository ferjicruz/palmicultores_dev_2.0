create table tbl_salidas
	(
		id integer not null primary key auto_increment,	
		folio_despacho int (15) null,
		fecha_despacho date null,
		cantidad decimal (13,2) null,
		um varchar (20) not null,
		peso_ton decimal (13,2) null,
		cliente_id integer not null,
		destino varchar (50) null,
		direccion varchar (50) null,
		almacen varchar (150) null,
		transportes varchar (50) null,
		carta_por_traslado varchar (50) null,
		fecha time null,
		operador varchar (50) null,
		licencia decimal (50) null,
		tractor_1 varchar (50) null,
		numero_1 int (15) null,
		placas_1 int (15) null,
		peso_bruto_1 decimal (13,2) null,
		peso_tara_1 decimal (13,2) null,
		peso_neto_aceite_1 decimal (50) null,
		remolque_2 varchar (50) null,
		numero_2 int (15) null,
		placas_2 int (15) null,
		peso_bruto_2 decimal (13,2) null,
		peso_tara_2 decimal (13,2) null,
		peso_neto_aceite_2 decimal (13,2) null,
		remolque_3 varchar (50) null,
		numero_3 int (15) null,
		placas_3 int (15) null,
		peso_bruto_3 decimal (13,2) null,
		peso_tara_3 decimal (13,2) null,
		peso_neto_aceite_3 decimal (13,2) null
	);

	alter table tbl_salidas
	    add constraint fk_cliente
		    foreign key (cliente_id)
			     references tbl_clientes (id);