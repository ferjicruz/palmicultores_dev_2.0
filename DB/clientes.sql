create table tbl_clientes
(
	id integer not null primary key auto_increment,
	nombre varchar(160)	NOT NULL,
	apellido_paterno varchar(160) null,
  	apellido_materno varchar(160) null,
	direccion varchar(160) null,
  	correo_electronico varchar(160) null,
	telefono varchar(160) null,
	rfc varchar(160) null
)