create table tbl_estados 
(
	id integer not null primary key auto_increment,
	clave char(2) NOT NULL,
	nombre varchar(160)	NOT NULL
);

create table tbl_municipios
(
	id integer not null primary key auto_increment,
	clave char(2) NOT NULL,
	nombre varchar(160) NOT NULL,
	estado_id integer NOT NULL
);

create table tbl_localidades
(
	id integer not null primary key auto_increment,
	clave char(4) NOT NULL,
	nombre varchar(160) NOT NULL,
	estado_id integer NOT NULL
);

create table tbl_productores
(
	id integer not null primary key auto_increment,
	clave numeric NOT NULL,
	nombre varchar(160) NOT NULL,
	apellido_paterno varchar(160) null,
  	apellido_materno varchar(160) null,
	estado_id integer null,
  	municipio_id integer null,
  	localidad_id integer null,
	no_parcelas integer NOT NULL,
  	curp varchar(18) NULL,
	rfc varchar(13) NULL,
 	calle varchar(160) NULL,
	no_exterior varchar(20) NULL,
	no_interior varchar(20) NULL,
 	colonia varchar(160) NULL,
	cp char(5) NULL,
	referencia varchar(160) NULL,
  	correo_electronico varchar(50) NULL,
	telefono integer(10) NULL,
	celular integer(10) NULL
);

create table tbl_sociedades
(
	id integer not null primary key auto_increment,
	razon_social varchar(160) NOT NULL,
	no_miembros integer NOT NULL,
	fecha_creacion datetime,
	estado_id integer NULL,
  	municipio_id integer NULL,
  	localidad_id integer NULL,
	no_parcelas integer NOT NULL,
	rfc numeric(13) NULL,
 	calle varchar(160) NULL,
	no_exterior varchar(20) NULL,
	no_interior varchar(20) NULL,
 	colonia varchar(160) NULL,
	cp char(5) NULL,
	referencia varchar(160) NULL,
  	correo_electronico varchar(160) NULL,
	telefono varchar(20) NULL,
	celular varchar(20) NULL,
	representante_legal varchar(160) NULL

create table tbl_centros_acopio 
(
	id integer not null primary key auto_increment,
	nombre text(160) NOT NULL,
	no_miembros integer NOT NULL,
	estado_id integer NULL,
  	municipio_id integer NULL,
  	localidad_id integer NULL,
	no_parcelas integer NOT NULL,
 	calle varchar(160) NULL,
	no_exterior varchar(20) NULL,
	no_interior varchar(20) NULL,
 	colonia varchar(160) NULL,
	codigo_postal int(5) NULL,
	referencia varchar(160) NULL,
  	correo_electronico varchar(160) NULL,
	telefono integer(10) NULL,
	celular integer(10) NULL;		
	
alter table tbl_localidades
  add constraint fk_estados_localidades
	foreign key (estado_id) 
		references tbl_estados (id);
		
alter table tbl_municipios
  add constraint fk_estados_municipios
	foreign key (estado_id) 
		references tbl_estados (id);

alter table tbl_productores
  add constraint fk_estado_productores
	foreign key (estado_id) 
		references tbl_estados (id);
		
alter table tbl_productores
  add constraint fk_municipio_productores
    foreign key (municipio_id)
	  references tbl_municipios (id);
	  
alter table tbl_productores
  add constraint fk_localidad_productores
	foreign key (localidad_id)
	  references tbl_localidades (id);
	  
alter table tbl_sociedades
  add constraint fk_estado_sociedades
	foreign key (estado_id) 
		references tbl_estados (id);
		
alter table tbl_sociedades
  add constraint fk_municipio_sociedades
    foreign key (municipio_id)
	  references tbl_municipios (id);
	  
alter table tbl_sociedades
  add constraint fk_localidad_sociedades
	foreign key (localidad_id)
	  references tbl_localidades (id);
	  
alter table tbl_centros_acopio
  add constraint fk_estado_centros_acopio
	foreign key (estado_id) 
		references tbl_estados (id);
		
alter table tbl_centros_acopio
  add constraint fk_municipio_centros_acopio
    foreign key (municipio_id)
	  references tbl_municipios (id);
	  
alter table tbl_centros_acopio
  add constraint fk_localidad_centros_acopio
	foreign key (localidad_id)
	  references tbl_localidades (id);
	  
	  