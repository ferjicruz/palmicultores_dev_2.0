<?php

namespace app\controllers;

use Yii;
use app\models\Entradas;
use app\models\EntradasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\filters\AccessControl;

/**
 * EntradasController implements the CRUD actions for Entradas model.
 */
class EntradasController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return 
		[
		'access'=>[
			'class'=>AccessControl::classname(),
			'only'=>['create','update','delete','view','index'],
			'rules'=>[
				[
					'allow'=>true,
					'roles'=>['@']
				],
				
			]
		
		
		],
		
		
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Entradas models.
     * @return mixed
     */
    public function actionIndex()
    {
		
        $searchModel = new EntradasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entradas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Entradas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Entradas();
		// Generador de entradas
		$num_entrada = (new \yii\db\Query())
		->select(['ultimo_folio'])
		->from('tbl_folios_entrada')
		->where('anio='.date('y').' and mes='.date('m'))
		->all();
		$model->num_entrada = implode($num_entrada);
		
		// Generador de folios
		$folio = (new \yii\db\Query())
		->select(['ultimo_folio'])
		->from(['tbl_folios_entrada'])
		->where('mes=0 and anio=0')
		->all();	
		//$model->folio = $folio;
		
      
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Entradas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Entradas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	public function actionPrint($id)
	{
        $model = $this->findModel($id);
		
		// get your HTML raw content without any layouts or scripts
		//$content = $this->renderPartial('_reportView');
		$content = "<!--Autor: Marroquín Morales Oscar Francisco --> 

<!DOCTYPE html>
<html>
<head>
	
</head>

	<p align=center><b> Palmicultores del
	Milenio del Estado de Campeche; U.S.P.R. de R.I.</b> </p>

	<p align=center> Sistema de Planeación de
	Recursos Empresariales (E.R.P.)</p>

	<p align=center> Boleta de Captura de
	Entrada del Producto.</p>


	


<body>

<div align=center>

<img align=center src=palm.png border=0> 
<br>
<br>
<br>
<br>

<br>

<div class='table-responsive'>

 <table class='table table-bordered'>


 
	    <tr>
			<td> <b>Productor:</b> $model->productor_id</td>
		</tr>


		<tr>	
			<td> <b>Domicilio:</b> </td>
		</tr>


		<tr>	
			<td> <b>Producto:</b> Semilla de Palma de Aceite </td>
		</tr>

	</table>

<br>

	 <table class='table table-bordered'>
	
	    <tr>
			<th> <thead>DETALLES DEL PESO </thead></th> 
			<th> <thead>SELLO</thead> </th> 
		</tr>


		<tr>
			<td> 
				 <b>Folio:</b>  $model->folio
				 <br>
				 <b>Fecha de Entrada:</b>  $model->fecha_entrada	
				 <br>
				 <b>Hora de Entrada:</b>  $model->hora_entrada  <th rowspan=\"2\"> </th> 
			</td>
		</tr>


		<tr>
			<td>
				 <b>No. de Entrada:</b> $model->hora_entrada
				 <br>
				 <b>Peso Bruto:</b>  $model->peso_bruto
				 <br>
				 <b>Peso Tara:</b>  $model->peso_tara
				 <br>
				 <b>Peso Neto:</b>
				 <br>
				 <b>Fecha Salida:</b>  $model->fecha_salida
				 <br>
				 <b>Hora Salida:</b>  $model->hora_salida
			</td>
		</tr>

	</table>

<br>

	 <table class='table table-bordered'>

		<tr>
			<td><b>Operador Bascula:</b>  $model->operador_bascula</td>
		</tr>

	</table>

<br>

	 <table class='table table-bordered'>

		<tr>
			<td><b>Chofer:</b>  $model->chofer</td>
		</tr>

	</table>

<br>

	 <table class='table table-bordered'>

		<tr>
			<td>
				<b>placas:</b>  $model->placas
				<br>
				<b>Caracteristicas del Vehiculo:</b>
			</td>
		</tr>

	</table>

<br>

	 <table class='table table-bordered'>

		<tr>
			<td><b>Observaciones:</b>  $model->observaciones</td>
		</tr>

	</table>


</div>

</body>
</html>";
 
		// setup kartik\mpdf\Pdf component
		$pdf = new Pdf([
			'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
			'content' => $content,//$this->renderPartial('privacy'),
			'options' => [
				'title' => 'Privacy Policy - Krajee.com',
				'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
			],
			'methods' => [
				'SetHeader' => ['Generated By: Krajee Pdf Component||Generated On: ' . date("r")],
				'SetFooter' => ['|Page {PAGENO}|'],
			]
		]);
		
		$pdf->cssFile = Yii::getAlias('@bower') . DIRECTORY_SEPARATOR . 'bootstrap' .
		DIRECTORY_SEPARATOR . 'dist' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR .
		'bootstrap.min.css';
		
		//return $pdf->render();		// return the pdf output as per the destination setting
		//exit;
		$pdf->Output($content,"entrada_$model->folio.pdf");
		//return $pdf->render(); 		
		exit;
		
	}

    /**
     * Finds the Entradas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entradas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entradas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
