<?php

namespace app\controllers;

use Yii;
use app\models\Salidas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


use kartik\mpdf\Pdf;


/**
 * SalidasController implements the CRUD actions for Salidas model.
 */
class SalidasController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return 
		[
		'access'=>[
			'class'=>AccessControl::classname(),
			'only'=>['create','update','delete','view','index'],
			'rules'=>[
				[
					'allow'=>true,
					'roles'=>['@']
				],
				
			]
		
		
		],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Salidas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Salidas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Salidas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Salidas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Salidas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Salidas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Salidas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	public function actionPrint($id)
	{
        $model = $this->findModel($id);
		
		// get your HTML raw content without any layouts or scripts
		//$content = $this->renderPartial('_reportView');
		$content = "<!--Autor: Marroquín Morales Oscar Francisco --> 

<!DOCTYPE html>
<html>
<head>
	
	<p align=center><b>Palmicultores del Milenio del Estado de Campeche; U.S.P.R. de R.I.</b> </p>

	<p align=center> Carretera Enrique Rodriguez Cano a Chicbul S/N </p>

	<P align=center> Loc. Enrique Rodriguez Cano, Carmen, Campeche </P>

	<p align=center> 
	Tel. (982) 824 1792 y (982) 824 1793 </p>
</head>

<br>

<body>

<div align=center>

<img align=center src=palm.png border=0> 
<br>

	<table class='table table-bordered'>

	<tr>

    <td> 
		<b>Folio de Despacho:</b>				
		<br>
		<b>Fecha de Despacho:</b>	
		<br>
		<b>Hora de Despacho:</b>   
	</td>

	</tr>

	</table>


<br>
	<table class='table table-bordered'>

  <tr>

  <td> 
		<b>Producto:</b>				
		<br>
		<b>Cantidad:</b>	
		<br>
		<b>UM:</b>   
		<br>
		<b>Peso en Toneladas:</b> 

	</td>

	<td>
		<b>Cliente:</b>
		<br>
		<br>
		<br>
		<b>RFC:</b>
	</td>

  </tr>

  </table>

  <br>
  
  
	<table class='table table-bordered'>
  <tr>

  <td> 
		<b>Origen:</b>	Planta Extractora de Aceite \"Don Jorge Mena Perez\"			
		<br>
		<b>Direccion:</b> Carretera Enrique Rodrigez Cano a Chicbul Km. 0.5 interior derecho 1.5 Km. Loc. Enrique Rodriguez Cano, Carmen, Campeche.
		<br>
		<b>Telefonos:</b> Tel. (982) 824 1792 y (982) 824 1793.
		
	</td>
	</tr>
	</table>

	<br>
	
	<div class='table-responsive'>
	<table class='table table-bordered'>

  <tr>

  <td> 
		<b>Destino:</b>			
		<br>
		<b>Direccion:</b>
		<br>
		<b>Telefonos:</b> 
		
	</td>
	</tr>
	</table>


	<br>
		<table class='table table-bordered'>

  <tr>

  <td> 
		<b>Transporte:</b>				
		<br>
		<b>Carta de Porte-Traslado:</b>	
		<br>
		<b>Fecha:</b>   
		<br>
		<b>Operador:</b> 
		<br>
		<b>Licencia:</b>

	</td>

	<td>
		<b>TRACTOR</b>
		<br>
		<b>Numero:</b>
		<br>
		<b>Placas</b>
		<br>
		<b>Sellos de Seguridad</b>
		<br>
		<b>Peso Bruto (Ton/Met):</b>
		<br>
		<b>Peso Tara (Ton/Met):</b>
		<br>
		<b>Peso Neto de Aceite (Ton/Met):</b>
	</td>

	<td>
		<b>REMOLQUE 1</b>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</td>

		<td>
		<b>REMOLQUE 2</b>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</td>

  </tr>

  </table>

  	<br>
	<table class='table table-bordered'>

  <tr>

    <th> 
		<b>FIRMA Y SELLO DE CONFORMIDAD</b>			
	</th>
	</tr>
	</table>
	
	<table class='table table-bordered'>

  <tr>

  <th> 
		<b>VENDEDOR</b>				
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		Palmicultores del Milenio <br>
		del Estado de Campeche; <br>
		U.S.P.R. de R.I. 
		
	</th>

	<th>
		<b>TRANSPORTE</b>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		
		
	</th>

	<th>
		<b>CLIENTE</b>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</th>
	</tr>
	</table>

</div>

</body>
</html>";
		
		
 
		// setup kartik\mpdf\Pdf component
		$pdf = new Pdf([
			'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
			'content' => $content,//$this->renderPartial('privacy'),
			'options' => [
				'title' => 'Privacy Policy - Krajee.com',
				'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
			],
			'methods' => [
				'SetHeader' => ['Generated By: Krajee Pdf Component||Generated On: ' . date("r")],
				'SetFooter' => ['|Page {PAGENO}|'],
			]
		]);
		
		$pdf->cssFile = Yii::getAlias('@bower') . DIRECTORY_SEPARATOR . 'bootstrap' .
		DIRECTORY_SEPARATOR . 'dist' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR .
		'bootstrap.min.css';
		
		//return $pdf->render();		// return the pdf output as per the destination setting
		//exit;
		$pdf->Output($content,"salida_$model->folio_despacho.pdf");
		//return $pdf->render(); 		
		exit;
		
	}

    /**
     * Finds the Salidas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Salidas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Salidas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
