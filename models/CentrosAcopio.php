<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_centros_acopio".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $no_miembros
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $localidad_id
 * @property integer $no_parcelas
 * @property string $calle
 * @property string $no_exterior
 * @property string $no_interior
 * @property string $colonia
 * @property string $cp
 * @property string $referencia
 * @property string $correo_electronico
 * @property string $telefono
 * @property string $celular
 *
 * @property TblEstados $estado
 * @property TblLocalidades $localidad
 * @property TblMunicipios $municipio
 */
class CentrosAcopio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_centros_acopio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'no_miembros', 'no_parcelas'], 'required'],
            [['no_miembros', 'estado_id', 'municipio_id', 'localidad_id', 'no_parcelas'], 'integer'],
            [['nombre', 'calle', 'colonia', 'referencia', 'correo_electronico'], 'string', 'max' => 160],
            [['no_exterior', 'no_interior', 'telefono', 'celular'], 'string', 'max' => 20],
            [['cp'], 'string', 'max' => 5],
            [['estado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estados::className(), 'targetAttribute' => ['estado_id' => 'id']],
            [['localidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Localidades::className(), 'targetAttribute' => ['localidad_id' => 'id']],
            [['municipio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Municipios::className(), 'targetAttribute' => ['municipio_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
		
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'no_miembros' => Yii::t('app', 'No Miembros'),
            'estado_id' => Yii::t('app', 'Estado '),
            'municipio_id' => Yii::t('app', 'Municipio '),
            'localidad_id' => Yii::t('app', 'Localidad '),
            'no_parcelas' => Yii::t('app', 'No Parcelas'),
            'calle' => Yii::t('app', 'Calle'),
            'no_exterior' => Yii::t('app', 'No Exterior'),
            'no_interior' => Yii::t('app', 'No Interior'),
            'colonia' => Yii::t('app', 'Colonia'),
            'cp' => Yii::t('app', 'Cp'),
            'referencia' => Yii::t('app', 'Referencia'),
            'correo_electronico' => Yii::t('app', 'Correo Electronico'),
            'telefono' => Yii::t('app', 'Telefono'),
            'celular' => Yii::t('app', 'Celular'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(TblEstados::className(), ['id' => 'estado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidad()
    {
        return $this->hasOne(TblLocalidades::className(), ['id' => 'localidad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipio()
    {
        return $this->hasOne(TblMunicipios::className(), ['id' => 'municipio_id']);
    }
}
