<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CentrosAcopio;

/**
 * CentrosAcopioSearch represents the model behind the search form about `app\models\CentrosAcopio`.
 */
class CentrosAcopioSearch extends CentrosAcopio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'no_miembros', 'estado_id', 'municipio_id', 'localidad_id', 'no_parcelas'], 'integer'],
            [['nombre', 'calle', 'no_exterior', 'no_interior', 'colonia', 'cp', 'referencia', 'correo_electronico', 'telefono', 'celular'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CentrosAcopio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'no_miembros' => $this->no_miembros,
            'estado_id' => $this->estado_id,
            'municipio_id' => $this->municipio_id,
            'localidad_id' => $this->localidad_id,
            'no_parcelas' => $this->no_parcelas,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'calle', $this->calle])
            ->andFilterWhere(['like', 'no_exterior', $this->no_exterior])
            ->andFilterWhere(['like', 'no_interior', $this->no_interior])
            ->andFilterWhere(['like', 'colonia', $this->colonia])
            ->andFilterWhere(['like', 'cp', $this->cp])
            ->andFilterWhere(['like', 'referencia', $this->referencia])
            ->andFilterWhere(['like', 'correo_electronico', $this->correo_electronico])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'celular', $this->celular]);

        return $dataProvider;
    }
}
