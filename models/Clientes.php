<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_clientes".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property string $direccion
 * @property string $correo_electronico
 * @property string $telefono
 * @property string $rfc
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_clientes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre', 'apellido_paterno', 'apellido_materno', 'direccion', 'correo_electronico', 'telefono', 'rfc'], 'string', 'max' => 160],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
            'direccion' => 'Direccion',
            'correo_electronico' => 'Correo Electronico',
            'telefono' => 'Telefono',
            'rfc' => 'Rfc',
        ];
    }
}
