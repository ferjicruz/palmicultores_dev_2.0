<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_entradas".
 *
 * @property integer $id
 * @property integer $productor_id
 * @property integer $sociedad_id
 * @property integer $centro_acopio_id
 * @property integer $num_entrada
 * @property integer $folio
 * @property string $fecha_entrada
 * @property string $hora_entrada
 * @property string $peso_bruto
 * @property string $peso_tara
 * @property string $fecha_salida
 * @property string $hora_salida
 * @property string $vehiculo
 * @property string $placas
 * @property string $chofer
 * @property string $operador_bascula
 * @property string $observaciones
 * @property string $status_pagada
 * @property integer $anio
 * @property integer $mes
 *
 * @property TblCentrosAcopio $centroAcopio
 * @property TblProductores $productor
 * @property TblSociedades $sociedad
 */
class Entradas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_entradas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productor_id', 'sociedad_id', 'centro_acopio_id', 'num_entrada', 'folio', 'anio', 'mes'], 'integer'],
            [['num_entrada', 'folio', 'fecha_entrada', 'hora_entrada', 'peso_bruto', 'peso_tara', 'vehiculo', 'placas', 'chofer', 'operador_bascula', 'observaciones', 'status_pagada'], 'required'],
            [['fecha_entrada', 'hora_entrada', 'fecha_salida', 'hora_salida'], 'safe'],
            [['peso_bruto', 'peso_tara'], 'number'],
            [['vehiculo', 'chofer', 'operador_bascula'], 'string', 'max' => 160],
            [['placas'], 'string', 'max' => 30],
            [['observaciones'], 'string', 'max' => 300],
            [['status_pagada'], 'string', 'max' => 1],
            [['centro_acopio_id'], 'exist', 'skipOnError' => true, 'targetClass' => CentrosAcopio::className(), 'targetAttribute' => ['centro_acopio_id' => 'id']],
            [['productor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Productores::className(), 'targetAttribute' => ['productor_id' => 'id']],
            [['sociedad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sociedades::className(), 'targetAttribute' => ['sociedad_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'productor_id' => Yii::t('app', 'Productor '),
            'sociedad_id' => Yii::t('app', 'Sociedad '),
            'centro_acopio_id' => Yii::t('app', 'Centro Acopio '),
            'num_entrada' => Yii::t('app', 'Num Entrada'),
            'folio' => Yii::t('app', 'Folio'),
            'fecha_entrada' => Yii::t('app', 'Fecha Entrada'),
            'hora_entrada' => Yii::t('app', 'Hora Entrada'),
            'peso_bruto' => Yii::t('app', 'Peso Bruto'),
            'peso_tara' => Yii::t('app', 'Peso Tara'),
            'fecha_salida' => Yii::t('app', 'Fecha Salida'),
            'hora_salida' => Yii::t('app', 'Hora Salida'),
            'vehiculo' => Yii::t('app', 'Vehiculo'),
            'placas' => Yii::t('app', 'Placas'),
            'chofer' => Yii::t('app', 'Chofer'),
            'operador_bascula' => Yii::t('app', 'Operador Bascula'),
            'observaciones' => Yii::t('app', 'Observaciones'),
            'status_pagada' => Yii::t('app', 'Status Pagada'),
            'anio' => Yii::t('app', 'Anio'),
            'mes' => Yii::t('app', 'Mes'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCentroAcopio()
    {
        return $this->hasOne(TblCentrosAcopio::className(), ['id' => 'centro_acopio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductor()
    {
        return $this->hasOne(TblProductores::className(), ['id' => 'productor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSociedad()
    {
        return $this->hasOne(TblSociedades::className(), ['id' => 'sociedad_id']);
    }
}
