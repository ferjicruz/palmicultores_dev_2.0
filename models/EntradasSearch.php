<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Entradas;

/**
 * EntradasSearch represents the model behind the search form about `app\models\Entradas`.
 */
class EntradasSearch extends Entradas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'productor_id', 'sociedad_id', 'centro_acopio_id', 'num_entrada', 'folio', 'anio', 'mes'], 'integer'],
            [['fecha_entrada', 'hora_entrada', 'fecha_salida', 'hora_salida', 'vehiculo', 'placas', 'chofer', 'operador_bascula', 'observaciones', 'status_pagada'], 'safe'],
            [['peso_bruto', 'peso_tara'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Entradas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'productor_id' => $this->productor_id,
            'sociedad_id' => $this->sociedad_id,
            'centro_acopio_id' => $this->centro_acopio_id,
            'num_entrada' => $this->num_entrada,
            'folio' => $this->folio,
            'fecha_entrada' => $this->fecha_entrada,
            'hora_entrada' => $this->hora_entrada,
            'peso_bruto' => $this->peso_bruto,
            'peso_tara' => $this->peso_tara,
            'fecha_salida' => $this->fecha_salida,
            'hora_salida' => $this->hora_salida,
            'anio' => $this->anio,
            'mes' => $this->mes,
        ]);

        $query->andFilterWhere(['like', 'vehiculo', $this->vehiculo])
            ->andFilterWhere(['like', 'placas', $this->placas])
            ->andFilterWhere(['like', 'chofer', $this->chofer])
            ->andFilterWhere(['like', 'operador_bascula', $this->operador_bascula])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'status_pagada', $this->status_pagada]);

        return $dataProvider;
    }
}
