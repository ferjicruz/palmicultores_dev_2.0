<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_estados".
 *
 * @property integer $id
 * @property string $clave
 * @property string $nombre
 *
 * @property TblCentrosAcopio[] $tblCentrosAcopios
 * @property TblLocalidades[] $tblLocalidades
 * @property TblMunicipios[] $tblMunicipios
 * @property TblProductores[] $tblProductores
 * @property TblSociedades[] $tblSociedades
 */
class Estados extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_estados';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clave', 'nombre'], 'required'],
            [['clave'], 'string', 'max' => 2],
            [['nombre'], 'string', 'max' => 160],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clave' => Yii::t('app', 'Clave'),
            'nombre' => Yii::t('app', 'Nombre')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblCentrosAcopios()
    {
        return $this->hasMany(TblCentrosAcopio::className(), ['estado_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblLocalidades()
    {
        return $this->hasMany(TblLocalidades::className(), ['estado_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblMunicipios()
    {
        return $this->hasMany(TblMunicipios::className(), ['estado_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblProductores()
    {
        return $this->hasMany(TblProductores::className(), ['estado_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblSociedades()
    {
        return $this->hasMany(TblSociedades::className(), ['estado_id' => 'id']);
    }
}
