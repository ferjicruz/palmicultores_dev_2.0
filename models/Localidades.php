<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_localidades".
 *
 * @property integer $id
 * @property string $clave
 * @property string $nombre
 * @property integer $estado_id
 *
 * @property TblCentrosAcopio[] $tblCentrosAcopios
 * @property TblEstados $estado
 * @property TblProductores[] $tblProductores
 * @property TblSociedades[] $tblSociedades
 */
class Localidades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_localidades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clave', 'nombre', 'estado_id'], 'required'],
            [['estado_id'], 'integer'],
            [['clave'], 'string', 'max' => 4],
            [['nombre'], 'string', 'max' => 160],
            [['estado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estados::className(), 'targetAttribute' => ['estado_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clave' => Yii::t('app', 'Clave'),
            'nombre' => Yii::t('app', 'Nombre'),
            'estado_id' => Yii::t('app', 'Estado '),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblCentrosAcopios()
    {
        return $this->hasMany(TblCentrosAcopio::className(), ['localidad_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(TblEstados::className(), ['id' => 'estado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblProductores()
    {
        return $this->hasMany(TblProductores::className(), ['localidad_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblSociedades()
    {
        return $this->hasMany(TblSociedades::className(), ['localidad_id' => 'id']);
    }
}
