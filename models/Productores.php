<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_productores".
 *
 * @property integer $id
 * @property string $clave
 * @property string $nombre
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $localidad_id
 * @property integer $no_parcelas
 * @property string $curp
 * @property string $rfc
 * @property string $calle
 * @property string $no_exterior
 * @property string $no_interior
 * @property string $colonia
 * @property string $cp
 * @property string $referencia
 * @property string $correo_electronico
 * @property string $telefono
 * @property string $celular
 *
 * @property TblEstados $estado
 * @property TblLocalidades $localidad
 * @property TblMunicipios $municipio
 */
class Productores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_productores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clave', 'nombre', 'no_parcelas'], 'required'],
            [['clave'], 'number'],
            [['estado_id', 'municipio_id', 'localidad_id', 'no_parcelas'], 'integer'],
            [['nombre', 'apellido_paterno', 'apellido_materno', 'calle', 'colonia', 'referencia', 'correo_electronico'], 'string', 'max' => 160],
            [['curp'], 'string', 'max' => 18],
            [['rfc'], 'string', 'max' => 13],
            [['no_exterior', 'no_interior', 'telefono', 'celular'], 'string', 'max' => 20],
            [['cp'], 'string', 'max' => 5],
            [['estado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estados::className(), 'targetAttribute' => ['estado_id' => 'id']],
            [['localidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Localidades::className(), 'targetAttribute' => ['localidad_id' => 'id']],
            [['municipio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Municipios::className(), 'targetAttribute' => ['municipio_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clave' => Yii::t('app', 'Clave'),
            'nombre' => Yii::t('app', 'Nombre'),
            'apellido_paterno' => Yii::t('app', 'Apellido Paterno'),
            'apellido_materno' => Yii::t('app', 'Apellido Materno'),
            'estado_id' => Yii::t('app', 'Estado ID'),
            'municipio_id' => Yii::t('app', 'Municipio'),
            'localidad_id' => Yii::t('app', 'Localidad'),
            'no_parcelas' => Yii::t('app', 'No Parcelas'),
            'curp' => Yii::t('app', 'Curp'),
            'rfc' => Yii::t('app', 'Rfc'),
            'calle' => Yii::t('app', 'Calle'),
            'no_exterior' => Yii::t('app', 'No Exterior'),
            'no_interior' => Yii::t('app', 'No Interior'),
            'colonia' => Yii::t('app', 'Colonia'),
            'cp' => Yii::t('app', 'Cp'),
            'referencia' => Yii::t('app', 'Referencia'),
            'correo_electronico' => Yii::t('app', 'Correo Electronico'),
            'telefono' => Yii::t('app', 'Telefono'),
            'celular' => Yii::t('app', 'Celular'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(TblEstados::className(), ['id' => 'estado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidad()
    {
        return $this->hasOne(TblLocalidades::className(), ['id' => 'localidad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMunicipio()
    {
        return $this->hasOne(TblMunicipios::className(), ['id' => 'municipio_id']);
    }
}
