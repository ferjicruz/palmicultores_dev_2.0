<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Productores;

/**
 * ProductoresSearch represents the model behind the search form about `app\models\Productores`.
 */
class ProductoresSearch extends Productores
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'estado_id', 'municipio_id', 'localidad_id', 'no_parcelas'], 'integer'],
            [['clave'], 'number'],
            [['nombre', 'apellido_paterno', 'apellido_materno', 'curp', 'rfc', 'calle', 'no_exterior', 'no_interior', 'colonia', 'cp', 'referencia', 'correo_electronico', 'telefono', 'celular'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Productores::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'clave' => $this->clave,
            'estado_id' => $this->estado_id,
            'municipio_id' => $this->municipio_id,
            'localidad_id' => $this->localidad_id,
            'no_parcelas' => $this->no_parcelas,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellido_paterno', $this->apellido_paterno])
            ->andFilterWhere(['like', 'apellido_materno', $this->apellido_materno])
            ->andFilterWhere(['like', 'curp', $this->curp])
            ->andFilterWhere(['like', 'rfc', $this->rfc])
            ->andFilterWhere(['like', 'calle', $this->calle])
            ->andFilterWhere(['like', 'no_exterior', $this->no_exterior])
            ->andFilterWhere(['like', 'no_interior', $this->no_interior])
            ->andFilterWhere(['like', 'colonia', $this->colonia])
            ->andFilterWhere(['like', 'cp', $this->cp])
            ->andFilterWhere(['like', 'referencia', $this->referencia])
            ->andFilterWhere(['like', 'correo_electronico', $this->correo_electronico])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'celular', $this->celular]);

        return $dataProvider;
    }
}
