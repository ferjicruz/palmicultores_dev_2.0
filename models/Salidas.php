<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_salidas".
 *
 * @property integer $id
 * @property integer $folio_despacho
 * @property string $fecha_despacho
 * @property string $cantidad
 * @property string $um
 * @property string $peso_ton
 * @property integer $cliente_id
 * @property string $destino
 * @property string $direccion
 * @property string $almacen
 * @property string $transportes
 * @property string $carta_por_traslado
 * @property string $fecha
 * @property string $operador
 * @property string $licencia
 * @property string $tractor_1
 * @property integer $numero_1
 * @property integer $placas_1
 * @property string $peso_bruto_1
 * @property string $peso_tara_1
 * @property string $peso_neto_aceite_1
 * @property string $remolque_2
 * @property integer $numero_2
 * @property integer $placas_2
 * @property string $peso_bruto_2
 * @property string $peso_tara_2
 * @property string $peso_neto_aceite_2
 * @property string $remolque_3
 * @property integer $numero_3
 * @property integer $placas_3
 * @property string $peso_bruto_3
 * @property string $peso_tara_3
 * @property string $peso_neto_aceite_3
 *
 * @property TblClientes $cliente
 */
class Salidas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_salidas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['folio_despacho', 'cliente_id', 'numero_1', 'placas_1', 'numero_2', 'placas_2', 'numero_3', 'placas_3'], 'integer'],
            [['fecha_despacho', 'fecha'], 'safe'],
            [['cantidad', 'peso_ton', 'licencia', 'peso_bruto_1', 'peso_tara_1', 'peso_neto_aceite_1', 'peso_bruto_2', 'peso_tara_2', 'peso_neto_aceite_2', 'peso_bruto_3', 'peso_tara_3', 'peso_neto_aceite_3'], 'number'],
            [['um', 'cliente_id'], 'required'],
            [['um'], 'string', 'max' => 20],
            [['destino', 'direccion', 'transportes', 'carta_por_traslado', 'operador', 'tractor_1', 'remolque_2', 'remolque_3'], 'string', 'max' => 50],
            [['almacen'], 'string', 'max' => 150],
            [['cliente_id'], 'exist', 'skipOnError' => true, 'targetClass' => clientes::className(), 'targetAttribute' => ['cliente_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'folio_despacho' => Yii::t('app', 'Folio Despacho'),
            'fecha_despacho' => Yii::t('app', 'Fecha Despacho'),
            'cantidad' => Yii::t('app', 'Cantidad'),
            'um' => Yii::t('app', 'Um'),
            'peso_ton' => Yii::t('app', 'Peso Ton'),
            'cliente_id' => Yii::t('app', 'Cliente ID'),
            'destino' => Yii::t('app', 'Destino'),
            'direccion' => Yii::t('app', 'Direccion'),
            'almacen' => Yii::t('app', 'Almacen'),
            'transportes' => Yii::t('app', 'Transportes'),
            'carta_por_traslado' => Yii::t('app', 'Carta Por Traslado'),
            'fecha' => Yii::t('app', 'Fecha'),
            'operador' => Yii::t('app', 'Operador'),
            'licencia' => Yii::t('app', 'Licencia'),
            'tractor_1' => Yii::t('app', 'Tractor 1'),
            'numero_1' => Yii::t('app', 'Numero 1'),
            'placas_1' => Yii::t('app', 'Placas 1'),
            'peso_bruto_1' => Yii::t('app', 'Peso Bruto 1'),
            'peso_tara_1' => Yii::t('app', 'Peso Tara 1'),
            'peso_neto_aceite_1' => Yii::t('app', 'Peso Neto Aceite 1'),
            'remolque_2' => Yii::t('app', 'Remolque 2'),
            'numero_2' => Yii::t('app', 'Numero 2'),
            'placas_2' => Yii::t('app', 'Placas 2'),
            'peso_bruto_2' => Yii::t('app', 'Peso Bruto 2'),
            'peso_tara_2' => Yii::t('app', 'Peso Tara 2'),
            'peso_neto_aceite_2' => Yii::t('app', 'Peso Neto Aceite 2'),
            'remolque_3' => Yii::t('app', 'Remolque 3'),
            'numero_3' => Yii::t('app', 'Numero 3'),
            'placas_3' => Yii::t('app', 'Placas 3'),
            'peso_bruto_3' => Yii::t('app', 'Peso Bruto 3'),
            'peso_tara_3' => Yii::t('app', 'Peso Tara 3'),
            'peso_neto_aceite_3' => Yii::t('app', 'Peso Neto Aceite 3'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(TblClientes::className(), ['id' => 'cliente_id']);
    }

    /**
     * @inheritdoc
     * @return SalidasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SalidasQuery(get_called_class());
    }
}
