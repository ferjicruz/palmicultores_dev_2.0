<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CentrosAcopioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="centros-acopio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'no_miembros') ?>

    <?= $form->field($model, 'estado_id') ?>

    <?= $form->field($model, 'municipio_id') ?>

    <?php // echo $form->field($model, 'localidad_id') ?>

    <?php // echo $form->field($model, 'no_parcelas') ?>

    <?php // echo $form->field($model, 'calle') ?>

    <?php // echo $form->field($model, 'no_exterior') ?>

    <?php // echo $form->field($model, 'no_interior') ?>

    <?php // echo $form->field($model, 'colonia') ?>

    <?php // echo $form->field($model, 'cp') ?>

    <?php // echo $form->field($model, 'referencia') ?>

    <?php // echo $form->field($model, 'correo_electronico') ?>

    <?php // echo $form->field($model, 'telefono') ?>

    <?php // echo $form->field($model, 'celular') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
