<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CentrosAcopio */

$this->title = 'Create Centros Acopio';
$this->params['breadcrumbs'][] = ['label' => 'Centros Acopios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="centros-acopio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
