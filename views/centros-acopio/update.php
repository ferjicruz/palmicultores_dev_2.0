<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CentrosAcopio */

$this->title = 'Update Centros Acopio: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Centros Acopios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="centros-acopio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
