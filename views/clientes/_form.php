<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clientes-form">
<div class = "container">
<div class="form-group row">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-6">
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_paterno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_materno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
</div>
<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-6">

    <?= $form->field($model, 'correo_electronico')->widget(\yii\widgets\MaskedInput::className(), [
    'name' => 'input-36',
    'clientOptions' => [
        'alias' =>  'email'
    ],
]);	?>

   <?= $form->field($model, 'telefono')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '999-999-9999',
]) ?>

    <?= $form->field($model, 'rfc')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
