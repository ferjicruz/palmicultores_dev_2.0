<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\timepicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use dosamigos\datepicker\DatePicker;
use app\models\Productores;
use app\models\Sociedades;
use app\models\CentrosAcopio;
/* @var $this yii\web\View */
/* @var $model app\models\Entradas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entradas-form">
<div class = "container">
<div class="form-group row">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-6">
     <?= $form->field($model, 'productor_id')->widget (Select2::classname(), [
   'data' => ArrayHelper::map( Productores::find()->all(), 'id', 'nombre'),
   'language' => 'en',
   'options' => ['placeholder' => 'Seleccionar Productor'],
   'pluginOptions' => [
   'allowClear' => true
   ],
   ]);
							

?>

   <?= $form->field($model, 'sociedad_id')->widget (Select2::classname(), [
   'data' => ArrayHelper::map( Sociedades::find()->all(), 'id', 'razon_social'),
   'language' => 'en',
   'options' => ['placeholder' => 'Seleccionar sociedad'],
   'pluginOptions' => [
   'allowClear' => true
   ],
   ]);
							

?>

 <?= $form->field($model, 'centro_acopio_id')->widget (Select2::classname(), [
   'data' => ArrayHelper::map( CentrosAcopio::find()->all(), 'id', 'nombre'),
   'language' => 'en',
   'options' => ['placeholder' => 'Seleccionar centro de acopio'],
   'pluginOptions' => [
   'allowClear' => true
   ],
   ]);
							

?>

    <?= $form->field($model, 'num_entrada')->textInput() ?>

    <?= $form->field($model, 'folio')->textInput() ?>

 
	<?= $form->field($model, 'fecha_entrada')->widget(DatePicker::classname(), [
      // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
]);?>

    <?= $form->field($model, 'hora_entrada')->widget(TimePicker::className(), [
	   'name' => 't1',
    'pluginOptions' => [
        'showSeconds' => false,
        'showMeridian' => false,
        'minuteStep' => 1,
    ]
]);?>

    <?= $form->field($model, 'peso_bruto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso_tara')->textInput(['maxlength' => true]) ?>


<?= $form->field($model, 'fecha_salida')->widget(DatePicker::className(), [
      // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
]);?>
	</div>
    
	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-6">
    
  <?= $form->field($model, 'hora_salida')->widget(TimePicker::className(), [
	   'name' => 't1',
    'pluginOptions' => [
        'showSeconds' => false,
        'showMeridian' => false,
        'minuteStep' => 1,
    ]
]);?>


    <?= $form->field($model, 'vehiculo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'placas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chofer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operador_bascula')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'observaciones')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_pagada')->textInput(['maxlength' => true]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
