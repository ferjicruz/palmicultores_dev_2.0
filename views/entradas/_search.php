<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EntradasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entradas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'productor_id') ?>

    <?= $form->field($model, 'sociedad_id') ?>

    <?= $form->field($model, 'centro_acopio_id') ?>

    <?= $form->field($model, 'num_entrada') ?>

    <?php // echo $form->field($model, 'folio') ?>

    <?php // echo $form->field($model, 'fecha_entrada') ?>

    <?php // echo $form->field($model, 'hora_entrada') ?>

    <?php // echo $form->field($model, 'peso_bruto') ?>

    <?php // echo $form->field($model, 'peso_tara') ?>

    <?php // echo $form->field($model, 'fecha_salida') ?>

    <?php // echo $form->field($model, 'hora_salida') ?>

    <?php // echo $form->field($model, 'vehiculo') ?>

    <?php // echo $form->field($model, 'placas') ?>

    <?php // echo $form->field($model, 'chofer') ?>

    <?php // echo $form->field($model, 'operador_bascula') ?>

    <?php // echo $form->field($model, 'observaciones') ?>

    <?php // echo $form->field($model, 'status_pagada') ?>

    <?php // echo $form->field($model, 'anio') ?>

    <?php // echo $form->field($model, 'mes') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
