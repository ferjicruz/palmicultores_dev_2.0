<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EntradasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entradas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Entradas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'productor_id',
            'sociedad_id',
            'centro_acopio_id',
            'num_entrada',
            // 'folio',
            // 'fecha_entrada',
            // 'hora_entrada',
            // 'peso_bruto',
            // 'peso_tara',
            // 'fecha_salida',
            // 'hora_salida',
            // 'vehiculo',
            // 'placas',
            // 'chofer',
            // 'operador_bascula',
            // 'observaciones',
            // 'status_pagada',
            // 'anio',
            // 'mes',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
