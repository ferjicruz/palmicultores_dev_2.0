<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Entradas */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Entradas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		
		<?= Html::a('Print', ['print', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'productor_id',
            'sociedad_id',
            'centro_acopio_id',
            'num_entrada',
            'folio',
            'fecha_entrada',
            'hora_entrada',
            'peso_bruto',
            'peso_tara',
            'fecha_salida',
            'hora_salida',
            'vehiculo',
            'placas',
            'chofer',
            'operador_bascula',
            'observaciones',
            'status_pagada',
            
        ],
    ]) ?>

</div>
