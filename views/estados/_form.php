<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Estados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estados-form">
<div class = "container">
<div class="form-group row">
    <?php $form = ActiveForm::begin(); ?>
    
	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-5">
    <?= $form->field($model, 'clave')->textInput(['maxlength' => true]) ?>
    </div>
	
	<div class="form-group-sm col-xs-4 col-sm-3 col-md-4 col-lg-5">
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    </div>
   
   
    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
