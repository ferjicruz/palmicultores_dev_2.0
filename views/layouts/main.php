<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\nav\NavX;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	
	
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Sistema de Planeación de Recursos Empresariales (ERP)',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
		
			  
        ],
    ]);
	 echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
			['label' => 'Catalogos',  'items' => [
                    ['label' => 'Centros de Acopio', 'url' => '/centros-acopio'],
					['label' => 'Clientes', 'url' => '/clientes'],
					
					['label' => 'Productores', 'url' => '/productores'], 
					['label' => 'Sociedades', 'url' => '/sociedades'], 
                    ],
                ],
			['label' => 'Boletas', 'items' => [
					['label' => 'Entradas', 'url' => '/entradas'],
					['label' => 'Salidas', 'url' => '/salidas'],
					]],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
			['label' => 'Report', 'url' => ['/site/contact']],
			
            Yii::$app->user->isGuest ? (
			['label' => 'Acceso', 'items' => [
					['label' => 'Signup', 'url' => '/site/signup'],
					['label' => 'Login' , 'url' => '/site/login'],
					],
			]
                
				
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
     /*echo NavX::widget([
    'options'=>['class'=>'nav nav-pills'],
    'items' => [
        ['label' => 'Inicio', 'url' => 'index.php'],
        ['label' => 'Catalogos', 'active'=>true, 'items' => [
            ['label' => 'Centros de Acopio', 'url' => 'index.php?r=centros-acopio'],
            ['label' => 'Clientes', 'url' => 'index.php?r=clientes'],
            ['label' => 'Estados', 'url' => 'index.php?r=estados'],       
			['label' => 'Localidades', 'url' => 'index.php?r=localidades'], 
			['label' => 'Municipios', 'url' => 'index.php?r=municipios'], 
			['label' => 'Productores', 'url' => 'index.php?r=productores'], 
			['label' => 'Sociedades', 'url' => 'index.php?r=sociedades'], 
        ]],
		['label' => 'Boletas', 'active'=>true, 'items' => [
            ['label' => 'Entradas', 'url' => 'index.php?r=entradas'],
        ]],
    ]
]);*/
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
		
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Palmicultores del Milenio © 2017 Desarrollado por
Alumnos del Instituto Tecnológico Superior de Escárcega, Generación 2014-2019 </p>

     
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>

 
	
<?php $this->endPage() ?>
