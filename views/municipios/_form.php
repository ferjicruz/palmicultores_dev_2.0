<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Estados;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Municipios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="municipios-form">
<div class = "container">
<div class="form-group row">
    <?php $form = ActiveForm::begin(); ?>
    
	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-3">
    <?= $form->field($model, 'clave')->textInput(['maxlength' => true]) ?>
    </div>
	
	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-3">
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    </div>
	
	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-3">
   <?= $form->field($model, 'estado_id')->widget (Select2::classname(), [
   'data' => ArrayHelper::map( Estados::find()->all(), 'id', 'nombre'),
   'language' => 'en',
   'options' => ['placeholder' => 'Seleccionar Estado'],
   'pluginOptions' => [
   'allowClear' => true
   ],
   ]);
							

?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
