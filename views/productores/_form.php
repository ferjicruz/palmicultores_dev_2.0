<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Estados;
use app\models\Municipios;
use app\models\Localidades;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Productores */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="productores-form">
<div class = "container">
<div class="form-group row">
    <?php $form = ActiveForm::begin(); ?>
	 
	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-6">
    <?= $form->field($model, 'clave')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
	
	
	
    <?= $form->field($model, 'apellido_paterno')->textInput(['maxlength' => true]) ?>
	
	
	
    <?= $form->field($model, 'apellido_materno')->textInput(['maxlength' => true]) ?>
	
 
	
       <?= $form->field($model, 'estado_id')->widget (Select2::classname(), [
   'data' => ArrayHelper::map( Estados::find()->all(), 'id', 'nombre'),
   'language' => 'en',
   'options' => ['placeholder' => 'Seleccionar Estado'],
   'pluginOptions' => [
   'allowClear' => true
   ],
   ]);
   ?>
   
   
	
    <?= $form->field($model, 'municipio_id')->widget (Select2::classname(), [
   'data' => ArrayHelper::map( Municipios::find()->all(), 'id', 'nombre'),
   'language' => 'en',
   'options' => ['placeholder' => 'Seleccionar Municipio'],
   'pluginOptions' => [
   'allowClear' => true
   ],
   ]);
   ?>
	

	
    <?= $form->field($model, 'localidad_id')->widget (Select2::classname(), [
   'data' => ArrayHelper::map( Localidades::find()->all(), 'id', 'nombre'),
   'language' => 'en',
   'options' => ['placeholder' => 'Seleccionar Localidad'],
   'pluginOptions' => [
   'allowClear' => true
   ],
   ]);
   ?>
   
   
	
    <?= $form->field($model, 'no_parcelas')->textInput(['maxlength' => true]) ?>
	
	
	
    <?= $form->field($model, 'curp')->textInput(['maxlength' => true]) ?>
	

	
    <?= $form->field($model, 'rfc')->textInput(['maxlength' => true]) ?>
    </div>
	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-6">
    <?= $form->field($model, 'calle')->textInput(['maxlength' => true]) ?>

	
	
    <?= $form->field($model, 'no_exterior')->textInput(['maxlength' => true]) ?>
	</div>
	
    <div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-6">
    <?= $form->field($model, 'no_interior')->textInput(['maxlength' => true]) ?>
	
	
	
    <?= $form->field($model, 'colonia')->textInput(['maxlength' => true]) ?>
	
	
	
    <?= $form->field($model, 'cp')->textInput(['maxlength' => true]) ?>
	
	
	
     <?= $form->field($model, 'referencia')->textInput(['maxlength' => true]) ?>
	
	 
	
    <?= $form->field($model, 'correo_electronico')->widget(\yii\widgets\MaskedInput::className(), [
    'name' => 'input-36',
    'clientOptions' => [
        'alias' =>  'email'
    ],
]);	?>
	

	
 <?= $form->field($model, 'telefono')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '999-999-9999',
]) ?>
	
	
 <?= $form->field($model, 'celular')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '999-999-9999',
]) ?>
	
	
    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
