<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Productores */

$this->title = 'Create Productores';
$this->params['breadcrumbs'][] = ['label' => 'Productores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
