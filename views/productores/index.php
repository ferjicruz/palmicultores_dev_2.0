<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Productores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'clave',
            'nombre',
            'apellido_paterno',
            'apellido_materno',
            // 'estado_id',
            // 'municipio_id',
            // 'localidad_id',
            // 'no_parcelas',
            // 'curp',
            // 'rfc',
            // 'calle',
            // 'no_exterior',
            // 'no_interior',
            // 'colonia',
            // 'cp',
            // 'referencia',
            // 'correo_electronico',
            // 'telefono',
            // 'celular',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
