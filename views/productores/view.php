<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Productores */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Productores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'clave',
            'nombre',
            'apellido_paterno',
            'apellido_materno',
            'estado_id',
            'municipio_id',
            'localidad_id',
            'no_parcelas',
            'curp',
            'rfc',
            'calle',
            'no_exterior',
            'no_interior',
            'colonia',
            'cp',
            'referencia',
            'correo_electronico',
            'telefono',
            'celular',
        ],
    ]) ?>

</div>
