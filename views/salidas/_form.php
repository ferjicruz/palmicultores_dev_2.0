<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Salidas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="salidas-form">
<div class = "container">
<div class="form-group row">


    <?php $form = ActiveForm::begin(); ?>

	<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-6">
    <?= $form->field($model, 'folio_despacho')->textInput() ?>

 	<?= $form->field($model, 'fecha_despacho')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
]);?>

    <?= $form->field($model, 'cantidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'um')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso_ton')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cliente_id')->textInput() ?>

    <?= $form->field($model, 'destino')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'almacen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transportes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carta_por_traslado')->textInput(['maxlength' => true]) ?>

     	<?= $form->field($model, 'fecha')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => false, 
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
]);?>

    <?= $form->field($model, 'operador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'licencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tractor_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_1')->textInput() ?>
</div>
<div class="form-group-sm col-xs-2 col-sm-3 col-md-4 col-lg-6">
    <?= $form->field($model, 'placas_1')->textInput() ?>

    <?= $form->field($model, 'peso_bruto_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso_tara_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso_neto_aceite_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remolque_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_2')->textInput() ?>

    <?= $form->field($model, 'placas_2')->textInput() ?>

    <?= $form->field($model, 'peso_bruto_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso_tara_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso_neto_aceite_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remolque_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numero_3')->textInput() ?>

    <?= $form->field($model, 'placas_3')->textInput() ?>

    <?= $form->field($model, 'peso_bruto_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso_tara_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso_neto_aceite_3')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
