<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Salidas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salidas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Salidas'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'folio_despacho',
            'fecha_despacho',
            'cantidad',
            'um',
            // 'peso_ton',
            // 'cliente_id',
            // 'destino',
            // 'direccion',
            // 'almacen',
            // 'transportes',
            // 'carta_por_traslado',
            // 'fecha',
            // 'operador',
            // 'licencia',
            // 'tractor_1',
            // 'numero_1',
            // 'placas_1',
            // 'peso_bruto_1',
            // 'peso_tara_1',
            // 'peso_neto_aceite_1',
            // 'remolque_2',
            // 'numero_2',
            // 'placas_2',
            // 'peso_bruto_2',
            // 'peso_tara_2',
            // 'peso_neto_aceite_2',
            // 'remolque_3',
            // 'numero_3',
            // 'placas_3',
            // 'peso_bruto_3',
            // 'peso_tara_3',
            // 'peso_neto_aceite_3',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
