<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Salidas */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salidas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salidas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
		<?= Html::a('Print', ['print', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'folio_despacho',
            'fecha_despacho',
            'cantidad',
            'um',
            'peso_ton',
            'cliente_id',
            'destino',
            'direccion',
            'almacen',
            'transportes',
            'carta_por_traslado',
            'fecha',
            'operador',
            'licencia',
            'tractor_1',
            'numero_1',
            'placas_1',
            'peso_bruto_1',
            'peso_tara_1',
            'peso_neto_aceite_1',
            'remolque_2',
            'numero_2',
            'placas_2',
            'peso_bruto_2',
            'peso_tara_2',
            'peso_neto_aceite_2',
            'remolque_3',
            'numero_3',
            'placas_3',
            'peso_bruto_3',
            'peso_tara_3',
            'peso_neto_aceite_3',
        ],
    ]) ?>

</div>
