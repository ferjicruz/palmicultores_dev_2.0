<?php

/* @var $this yii\web\View */

$this->title = 'Palmicultores';
?>

<div class="site-index">

    <div class="jumbotron">
        <h2>Bienvenidos al Sistema de Planeación de Recursos Empresariales (ERP)</h2>
		<h3>Palmicultores del Milenio del Estado de Campeche; U.S.P.R de R.I<h3>
		<h4>Presidente del Consejo de Administración: "C. Cecilio Aguirre Ramírez"<h4>

        <p class="lead"></p>
		
	
	</div>

    <div class="body-content">
		
		
		<div class="row">
		
			
		
			<div class="col-lg-3">
		
		<p>
		<?= \yii\helpers\Html::a('CENTROS DE ACOPIO', \yii\helpers\Url::to(['/centros-acopio']), ['class' => 'btn btn-lg btn-info']) ?>
		</p>
		
		<p>
		<?= \yii\helpers\Html::a('SOCIEDADES', \yii\helpers\Url::to(['/sociedades']), ['class' => 'btn btn-lg btn-info']) ?>
		</p>
		
		<p>
		<?= \yii\helpers\Html::a('PRODUCTORES', \yii\helpers\Url::to(['/productores']), ['class' => 'btn btn-lg btn-info']) ?>
		</p>
		
		<p>
		<?= \yii\helpers\Html::a('CLIENTES', \yii\helpers\Url::to(['/clientes']), ['class' => 'btn btn-lg btn-info']) ?>
		</p>
		
		</div>
	
		
			<div class="col-lg-3">
			
		<p>
		<?= \yii\helpers\Html::a('ENTRADAS', \yii\helpers\Url::to(['/entradas']), ['class' => 'btn btn-lg btn-success']) ?>
		</p>
		
		
		<p>
		<?= \yii\helpers\Html::a('SALIDAS', \yii\helpers\Url::to(['/salidas']), ['class' => 'btn btn-lg btn-success']) ?>
		</p>
		
		</div>
		
		<div class="col-lg-3">
		
		<center><img  src="/imagen/N.png" /><right/>
		
	
			
        </div>
        </div>

    </div>
	 
</div>
