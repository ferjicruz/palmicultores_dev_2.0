<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SociedadesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sociedades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sociedades-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sociedades', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'razon_social',
            'no_miembros',
            'fecha_creacion',
            'estado_id',
            // 'municipio_id',
            // 'localidad_id',
            // 'no_parcelas',
            // 'rfc',
            // 'calle',
            // 'no_exterior',
            // 'no_interior',
            // 'colonia',
            // 'cp',
            // 'referencia',
            // 'correo_electronico',
            // 'telefono',
            // 'celular',
            // 'representante_legal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
