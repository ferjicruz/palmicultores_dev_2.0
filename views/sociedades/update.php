<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sociedades */

$this->title = 'Update Sociedades: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sociedades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sociedades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
